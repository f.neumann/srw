Для тренировки:

1)Устанавливаем путь к тренировочному набору в train.py:
myGene = trainGenerator(2,'data/my_data/train','captured','ground_truth',data_gen_args,save_to_dir = None)

2)если обучаем с нуля, то раскоменчиваем код сразу после model.compile:

model.compile(generator_optimizer, loss='binary_crossentropy', metrics=['accuracy'])
model_checkpoint = ModelCheckpoint('unet_membrane.hdf5', monitor='loss',verbose=1, save_best_only=True)
model.fit_generator(myGene,steps_per_epoch=321,epochs=5,callbacks=[model_checkpoint])

и закомменчиваем:

#model.load_weights('unet_40.hdf5')

если требуется запустить с контрольной точки, то закомменчиваем:

#model_checkpoint = ModelCheckpoint('unet_membrane.hdf5', monitor='loss',verbose=1, save_best_only=True)
#model.fit_generator(myGene,steps_per_epoch=321,epochs=5,callbacks=[model_checkpoint])

и устанавливаем путь в:

model.load_weights('unet_40.hdf5')

3)устанавливаем файл чекпоинта

model_checkpoint = ModelCheckpoint('unet_40.hdf5', monitor='loss',verbose=1, save_best_only=False)

4)задаем количество фотографий, участвующих в обучении и количество эпох:

model.fit_generator(myGene,steps_per_epoch=321,epochs=20,callbacks=[model_checkpoint])

5)Запускаем:

python3 train.py

Для теста:

1)Устанавливаем путь к контрольной точке в test.py:

model.load_weights('unet_60.hdf5')

2)Устанавливаем путь тестовой выборки:

testGene = testGenerator("data/my_data/test2")

3)Устанавливаем количество изображений, которые будут использованы:

results = model.predict_generator(testGene,29,verbose=1)

4)Устанавливаем путь для сохранения итоговых изображений:

saveResult("data/my_data/test1",results)

5)Запускаем:

python3 test.py
