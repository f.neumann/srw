import os
import glob
#from PSNR import psnr_doe
#import pandas as pd
import numpy as np
from scipy.misc import imread
import math
input_path = '/mnt/data/nstud2/unet/data/my_data/test2'
gan_path = '/mnt/data/nstud2/unet/data/my_data/result_dual'
gt_path = '/mnt/data/nstud2/unet/data/my_data/test_gr_tr'


def psnr_doe(target, ref):
    target_data = np.array(target)
    ref_data = np.array(ref)

    diff = ref_data - target_data
    diff = diff.flatten('C')
    rmse = math.sqrt(np.mean(diff ** 2.))
    if (rmse == 0):
        return "images are identific"
    return 20 * math.log10(255.0 / rmse)

file_list = os.listdir(gan_path)
row1 = []
row2 = []
for file in file_list:


    idx = file.find("_reconstructed")
    input = imread(os.path.join(input_path, file[0:idx]+".png")).astype(np.float)
    gt = imread(os.path.join(gt_path, file[0:idx]+".png")).astype(np.float)
    gan = imread(os.path.join(gan_path, file)).astype(np.float)


    psnr_input = psnr_doe(input, gt)
    psnr_gan = psnr_doe(gan, gt)

    row1.append(psnr_input)
    row2.append(psnr_gan)

#print(np.mean(row1))
#print(np.mean(row2))
f = open('psnr.txt', 'a')
s = str(np.mean(row2))
f.write(s + '\n')
f.close()

