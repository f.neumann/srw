from model_p2p_2 import *
from keras.optimizers import *
from data_2 import *
import tensorflow as tf
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

data_gen_args = dict(rotation_range=0.2,
                    width_shift_range=0.05,
                    height_shift_range=0.05,
                    shear_range=0.05,
                    zoom_range=0.05,
                    horizontal_flip=True,
                    fill_mode='nearest')

# путь к папкам
path_to_data = '/mnt/data/nstud2/unet/data/my_data/train'

myGene = trainGenerator(2,path_to_data,'captured','ground_truth',data_gen_args,save_to_dir = None)
model = Generator()
generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
model.compile(generator_optimizer, loss='mse', metrics=['accuracy'])
#если есть, загружаем предобученную модель
#model.load_weights('unet_2_5.hdf5')
#ставим чекпоинт
file_checkpint = 'unet_10.hdf5'
model_checkpoint = ModelCheckpoint(file_checkpint, monitor='loss',verbose=1, save_best_only=False)
#количество изображений из списка, количество эпох
model.fit_generator(myGene,steps_per_epoch=321,epochs=1,callbacks=[model_checkpoint])
