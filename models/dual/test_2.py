from model_p2p import *
#from model import *
from keras.optimizers import *
from data import *
import tensorflow as tf
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

model = Generator()
generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
model.compile(generator_optimizer, loss='binary_crossentropy', metrics=['accuracy'])

#model.load_weights('backup/unet_50.hdf5')
model.load_weights('unet_2_5.hdf5')
path_to_data = '/mnt/data/nstud2/unet/data/my_data/test2'


#testGene = testGenerator("data/my_data/newtest")
testGene = testGenerator(path_to_data)
#testGene = testGenerator("data/my_data/newtest")
path_to_save = '/mnt/data/nstud2/unet/data/my_data/result_dual'
results = model.predict_generator(testGene,29,verbose=1)
saveResult(path_to_save,results)
