from model_p2p import *
#from model import *
from keras.optimizers import *
from data import *
import tensorflow as tf
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

model = Generator()
generator_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
model.compile(generator_optimizer, loss='mse', metrics=['accuracy'])
path_to_data = '/mnt/data/nstud2/unet/data/my_data/test2'

#model.load_weights('backup/unet_50.hdf5')
model.load_weights('unet_200.hdf5')

#testGene = testGenerator("data/my_data/newtest")
testGene = testGenerator(path_to_data)
#testGene = testGenerator("data/my_data/newtest")
path_to_save = '/mnt/data/nstud2/unet/data/my_data/result_mono'
results = model.predict_generator(testGene, 5,verbose=1)
saveResult(path_to_save,results)
