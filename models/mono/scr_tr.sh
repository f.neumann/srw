#!/bin/bash
for ((i=1; i < 10; i++))
do
python3 /mnt/data/nstud2/unet/models/mono/train_2.py
python3 /mnt/data/nstud2/unet/models/mono/test_2.py
python3 /mnt/data/nstud2/unet/models/mono/calc_psnr_3ch_2.py

let "a=50+5*$i"
cat unet_2_5.hdf5 > unet_2_$a.hdf5

done